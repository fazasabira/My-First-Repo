from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return render(request, 'lab_4/My Profile.html')

def jastip(request):
    return render(request, 'lab_4/jastip.html')

def jastiphto(request):
    return render(request, 'lab_4/jastiphto.html')

def jastippict(request):
    return render(request, 'lab_4/jastippict.html')

def jastipabout(request):
    return render(request, 'lab_4/jastipabout.html')

def formmember(request):
    return render(request, 'lab_4/formmember.html')

