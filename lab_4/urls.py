from django.urls import path
from .views import index, jastip, jastipabout, jastiphto, jastippict, formmember

urlpatterns = [
    path('', index, name='index'),
    path('jastip/', jastip, name='jastip'),
    path('jastip/about/', jastipabout, name='about'),
    path('jastip/hto/',jastiphto, name='hto'),
    path('jastip/pict/',jastippict, name='pict'),
    path('formmember/',formmember, name=''),

]